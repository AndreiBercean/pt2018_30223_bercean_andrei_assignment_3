package dataAccessLayer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.*;

public class ProductDAO extends AbstractDAO<product> 
{
	protected List<product> createObjects(ResultSet resultSet)
	{
		List<product> list = new ArrayList<product>();
			try {
				while(resultSet.next())
				{
					int id = (Integer) resultSet.getObject("id");
					String name = (String) resultSet.getObject("name");
					list.add(new product(id,name));
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return list;
	}
}
