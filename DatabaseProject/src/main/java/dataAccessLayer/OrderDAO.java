package dataAccessLayer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.*;

public class OrderDAO extends AbstractDAO<orders> 
{
	protected List<orders> createObjects(ResultSet resultSet)
	{
		List<orders> list = new ArrayList<orders>();
			try {
				while(resultSet.next())
				{
					int id = (Integer) resultSet.getObject("id");
					int id_c = (Integer) resultSet.getObject("id_customer");
					int id_p = (Integer) resultSet.getObject("id_product");
					int q = (Integer) resultSet.getObject("quantity");
					list.add(new orders(id,id_c,id_p,q));
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return list;
	}
}
