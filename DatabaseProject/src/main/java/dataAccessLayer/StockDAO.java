package dataAccessLayer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.*;

public class StockDAO extends AbstractDAO<stock> 
{
	protected List<stock> createObjects(ResultSet resultSet)
	{
		List<stock> list = new ArrayList<stock>();
			try {
				while(resultSet.next())
				{
					int id = (Integer) resultSet.getObject("id");
					int am = (Integer) resultSet.getObject("ammount");
					list.add(new stock(id,am));
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return list;
	}
}
