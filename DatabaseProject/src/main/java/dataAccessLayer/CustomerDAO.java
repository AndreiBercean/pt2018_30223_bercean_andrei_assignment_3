package dataAccessLayer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.*;

public class CustomerDAO extends AbstractDAO<customer> 
{
	protected List<customer> createObjects(ResultSet resultSet)
	{
		List<customer> list = new ArrayList<customer>();
			try {
				while(resultSet.next())
				{
					int id = (Integer) resultSet.getObject("id");
					String name = (String) resultSet.getObject("name");
					String email = (String) resultSet.getObject("email");
					list.add(new customer(id,name,email));
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return list;
	}
}
