package presentation;

import dataAccessLayer.*;
import businessLayer.*;
import model.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

public class GUI 
{
	
	static void populate(DefaultTableModel model, ArrayList<String[]> rawData)
	{
		for(String[] s : rawData)
		{
			model.addRow(s);
		}
	}
	
	public static void main(String[] args) 
	{
		final Bridge brain = new Bridge();
		
		final JFrame people = new JFrame("People");
		people.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		people.setSize(500,300);
		final JFrame order = new JFrame("People");
		order.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		order.setSize(500,300);
		final JFrame products = new JFrame("Account");
		products.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		products.setSize(500,300);
		
		final Color MAROON = new Color(128, 0, 0);
		final JFrame bank = new JFrame("Bank");
		JPanel controlPane = new JPanel();
		JPanel mainPane = new JPanel();
		bank.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		bank.setSize(600,500);
		
		mainPane.setLayout(new GridLayout());
		mainPane.setBackground(Color.LIGHT_GRAY);
		mainPane.setForeground(Color.BLACK);
		
		
		//-=Tabel=-//	
		final JTable table = new JTable();
		JScrollPane tabelPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		mainPane.add(tabelPane);
		//------//
		
		//-=Control=-//
		controlPane.setLayout(null);
		controlPane.setBackground(Color.LIGHT_GRAY);
		controlPane.setForeground(Color.BLACK);
		
		JButton seeClientButton = new JButton("Clients");
		seeClientButton.setBounds(30,10,100,35);
		seeClientButton.setBackground(Color.DARK_GRAY);
		seeClientButton.setForeground(Color.WHITE);
		seeClientButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				customerBuilder cb = new customerBuilder();
				DefaultTableModel model = cb.Model();
				populate(model,brain.viewCustomers());
				table.setModel(model);
				table.repaint();
			}
		});
		controlPane.add(seeClientButton);
		
		JButton seeProductsButton = new JButton("Products");
		seeProductsButton.setBounds(140,10,100,35);
		seeProductsButton.setBackground(Color.DARK_GRAY);
		seeProductsButton.setForeground(Color.WHITE);
		seeProductsButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				productBuilder pb = new productBuilder();
				DefaultTableModel model = pb.Model();
				populate(model,brain.viewProducts());
				table.setModel(model);
				table.repaint();
			}
		});
		controlPane.add(seeProductsButton);
		
		JButton seeStocksButton = new JButton("Stocks");
		seeStocksButton.setBounds(30,50,100,35);
		seeStocksButton.setBackground(Color.DARK_GRAY);
		seeStocksButton.setForeground(Color.WHITE);
		seeStocksButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				stockBuilder pb = new stockBuilder();
				DefaultTableModel model = pb.Model();
				populate(model,brain.viewStocks());
				table.setModel(model);
				table.repaint();
			}
		});
		controlPane.add(seeStocksButton);
		
		JButton seeOrdersButton = new JButton("Orders");
		seeOrdersButton.setBounds(140,50,100,35);
		seeOrdersButton.setBackground(Color.DARK_GRAY);
		seeOrdersButton.setForeground(Color.WHITE);
		seeOrdersButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				orderBuilder pb = new orderBuilder();
				DefaultTableModel model = pb.Model();
				populate(model,brain.viewOrders());
				table.setModel(model);
				table.repaint();
			}
		});
		controlPane.add(seeOrdersButton);
		
		JButton orderButton = new JButton("Order");
		orderButton.setBounds(70,115,150,35);
		orderButton.setBackground(MAROON);
		orderButton.setForeground(Color.WHITE);
		orderButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				//ADD//
				order.setSize(500,300);
				order.setLayout(null);
				JLabel ADDLabel = new JLabel("ORDER");
				ADDLabel.setBounds(10, 10, 90, 20);
				order.add(ADDLabel);
				
				JLabel CNPLabel = new JLabel("ID produs");
				CNPLabel.setBounds(10, 40, 30, 30);
				order.add(CNPLabel);
				final JTextField CNPBox  = new JTextField();
				CNPBox.setBounds(10,70,30,30);
				CNPBox.setBackground(Color.WHITE);
				CNPBox.setForeground(Color.BLACK);
				order.add(CNPBox);
				
				JLabel EmailLabel = new JLabel("ID client");
				EmailLabel.setBounds(60, 40, 50, 30);
				order.add(EmailLabel);
				final JTextField EmailBox  = new JTextField();
				EmailBox.setBounds(60,70,50,30);
				EmailBox.setBackground(Color.WHITE);
				EmailBox.setForeground(Color.BLACK);
				order.add(EmailBox);
				
				JLabel AddrLabel = new JLabel("Cantitate");
				AddrLabel.setBounds(120, 40, 50, 30);
				order.add(AddrLabel);
				final JTextField AddrBox  = new JTextField();
				AddrBox.setBounds(120,70,50,30);
				AddrBox.setBackground(Color.WHITE);
				AddrBox.setForeground(Color.BLACK);
				order.add(AddrBox);
				
				JButton addPerson = new JButton("ADD");
				addPerson.setBounds(250,70,100,30);
				addPerson.setBackground(MAROON);
				addPerson.setForeground(Color.WHITE);
				addPerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						brain.order(new product(Integer.parseInt(CNPBox.getText()),""), new customer(Integer.parseInt(EmailBox.getText()),"",""),Integer.parseInt(AddrBox.getText()));
						orderBuilder cb = new orderBuilder();
						DefaultTableModel model = cb.Model();
						populate(model,brain.viewOrders());
						table.setModel(model);
						table.repaint();
					}
				});
				order.add(addPerson);
				
				order.setResizable(false);
				order.setVisible(true);
				products.dispose();
				people.dispose();
			}
		});
		controlPane.add(orderButton);
		
		
		JButton personButton = new JButton("Manage Customers");
		personButton.setBounds(70,160,150,35);
		personButton.setBackground(MAROON);
		personButton.setForeground(Color.WHITE);
		personButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				//ADD//
				people.setSize(500,300);
				people.setLayout(null);
				JLabel ADDLabel = new JLabel("CUSTOMERS");
				ADDLabel.setBounds(10, 10, 90, 20);
				people.add(ADDLabel);
				
				JLabel NameLabel = new JLabel("Name");
				NameLabel.setBounds(60, 40, 50, 30);
				people.add(NameLabel);
				final JTextField NameBox  = new JTextField();
				NameBox.setBounds(60,70,50,30);
				NameBox.setBackground(Color.WHITE);
				NameBox.setForeground(Color.BLACK);
				people.add(NameBox);
				
				JLabel EmailLabel = new JLabel("Email");
				EmailLabel.setBounds(120, 40, 50, 30);
				people.add(EmailLabel);
				final JTextField EmailBox  = new JTextField();
				EmailBox.setBounds(120,70,50,30);
				EmailBox.setBackground(Color.WHITE);
				EmailBox.setForeground(Color.BLACK);
				people.add(EmailBox);
				
				JButton addPerson = new JButton("ADD");
				addPerson.setBounds(250,70,100,30);
				addPerson.setBackground(MAROON);
				addPerson.setForeground(Color.WHITE);
				addPerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						brain.addCustomer(new customer(0,NameBox.getText(),EmailBox.getText()));
						customerBuilder cb = new customerBuilder();
						DefaultTableModel model = cb.Model();
						populate(model,brain.viewCustomers());
						table.setModel(model);
						table.repaint();
					}
				});
				people.add(addPerson);
				
				//EDIT//
				JLabel CNPLabelE = new JLabel("ID");
				CNPLabelE.setBounds(10, 90, 30, 30);
				people.add(CNPLabelE);
				final JTextField CNPBoxE  = new JTextField();
				CNPBoxE.setBounds(10,120,30,30);
				CNPBoxE.setBackground(Color.WHITE);
				CNPBoxE.setForeground(Color.BLACK);
				people.add(CNPBoxE);
				
				JLabel NameLabelE = new JLabel("Name");
				NameLabelE.setBounds(60, 90, 50, 30);
				people.add(NameLabelE);
				final JTextField NameBoxE  = new JTextField();
				NameBoxE.setBounds(60,120,50,30);
				NameBoxE.setBackground(Color.WHITE);
				NameBoxE.setForeground(Color.BLACK);
				people.add(NameBoxE);
				
				JLabel AddrLabelE = new JLabel("Email");
				AddrLabelE.setBounds(120, 90, 50, 30);
				people.add(AddrLabelE);
				final JTextField EmailBoxE  = new JTextField();
				EmailBoxE.setBounds(120,120,50,30);
				EmailBoxE.setBackground(Color.WHITE);
				EmailBoxE.setForeground(Color.BLACK);
				people.add(EmailBoxE);
				
				JButton editPerson = new JButton("EDIT");
				editPerson.setBounds(250,120,100,30);
				editPerson.setBackground(MAROON);
				editPerson.setForeground(Color.WHITE);
				editPerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						brain.editCustomer(new customer(Integer.parseInt(CNPBoxE.getText()),NameBox.getText(),EmailBoxE.getText()));
						customerBuilder cb = new customerBuilder();
						DefaultTableModel model = cb.Model();
						populate(model,brain.viewCustomers());
						table.setModel(model);
						table.repaint();
					}
				});
				people.add(editPerson);
				
				//DELETE//
				JLabel CNPLabelD = new JLabel("ID");
				CNPLabelD.setBounds(10, 140, 30, 30);
				people.add(CNPLabelD);
				final JTextField CNPBoxD  = new JTextField();
				CNPBoxD.setBounds(10,180,30,30);
				CNPBoxD.setBackground(Color.WHITE);
				CNPBoxD.setForeground(Color.BLACK);
				people.add(CNPBoxD);
				
				JButton deletePerson = new JButton("DELETE");
				deletePerson.setBounds(60,180,100,30);
				deletePerson.setBackground(MAROON);
				deletePerson.setForeground(Color.WHITE);
				deletePerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						brain.deleteCustomer(new customer(Integer.parseInt(CNPBoxD.getText()),"",""));
						customerBuilder cb = new customerBuilder();
						DefaultTableModel model = cb.Model();
						populate(model,brain.viewCustomers());
						table.setModel(model);
						table.repaint();
					}
				});
				people.add(deletePerson);
				//------//
				
				people.setResizable(false);
				people.setVisible(true);
				products.dispose();
				order.dispose();
			}
		});
		controlPane.add(personButton);
		
		JButton accountButton = new JButton("Manage Products");
		accountButton.setBounds(70,200,150,35);
		accountButton.setBackground(MAROON);
		accountButton.setForeground(Color.WHITE);
		accountButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				//ADD//
				products.setSize(500,300);
				products.setLayout(null);
				JLabel ADDLabel = new JLabel("PRODUCTS");
				ADDLabel.setBounds(10, 10, 90, 20);
				products.add(ADDLabel);
				
				JLabel NameLabel = new JLabel("Name");
				NameLabel.setBounds(60, 40, 50, 30);
				products.add(NameLabel);
				final JTextField NameBox  = new JTextField();
				NameBox.setBounds(60,70,50,30);
				NameBox.setBackground(Color.WHITE);
				NameBox.setForeground(Color.BLACK);
				products.add(NameBox);
				
				JButton addPerson = new JButton("ADD");
				addPerson.setBounds(250,70,100,30);
				addPerson.setBackground(MAROON);
				addPerson.setForeground(Color.WHITE);
				addPerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						brain.addProduct(new product(0,NameBox.getText()));
						customerBuilder cb = new customerBuilder();
						DefaultTableModel model = cb.Model();
						populate(model,brain.viewCustomers());
						table.setModel(model);
						table.repaint();
					}
				});
				products.add(addPerson);
				
				//EDIT//
				JLabel CNPLabelE = new JLabel("ID");
				CNPLabelE.setBounds(10, 90, 30, 30);
				products.add(CNPLabelE);
				final JTextField CNPBoxE  = new JTextField();
				CNPBoxE.setBounds(10,120,30,30);
				CNPBoxE.setBackground(Color.WHITE);
				CNPBoxE.setForeground(Color.BLACK);
				products.add(CNPBoxE);
				
				JLabel NameLabelE = new JLabel("Name");
				NameLabelE.setBounds(60, 90, 50, 30);
				products.add(NameLabelE);
				final JTextField NameBoxE  = new JTextField();
				NameBoxE.setBounds(60,120,50,30);
				NameBoxE.setBackground(Color.WHITE);
				NameBoxE.setForeground(Color.BLACK);
				products.add(NameBoxE);
				
				JLabel AddrLabelE = new JLabel("Stock");
				AddrLabelE.setBounds(120, 90, 50, 30);
				products.add(AddrLabelE);
				final JTextField EmailBoxE  = new JTextField();
				EmailBoxE.setBounds(120,120,50,30);
				EmailBoxE.setBackground(Color.WHITE);
				EmailBoxE.setForeground(Color.BLACK);
				products.add(EmailBoxE);
				
				JButton editPerson = new JButton("EDIT");
				editPerson.setBounds(250,120,100,30);
				editPerson.setBackground(MAROON);
				editPerson.setForeground(Color.WHITE);
				editPerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						brain.editProduct(new product(Integer.parseInt(CNPBoxE.getText()),NameBox.getText()), new stock(Integer.parseInt(CNPBoxE.getText()), Integer.parseInt(EmailBoxE.getText())));
						customerBuilder cb = new customerBuilder();
						DefaultTableModel model = cb.Model();
						populate(model,brain.viewCustomers());
						table.setModel(model);
						table.repaint();
					}
				});
				products.add(editPerson);
				
				//DELETE//
				JLabel CNPLabelD = new JLabel("ID");
				CNPLabelD.setBounds(10, 140, 30, 30);
				products.add(CNPLabelD);
				final JTextField CNPBoxD  = new JTextField();
				CNPBoxD.setBounds(10,180,30,30);
				CNPBoxD.setBackground(Color.WHITE);
				CNPBoxD.setForeground(Color.BLACK);
				products.add(CNPBoxD);
				
				JButton deletePerson = new JButton("DELETE");
				deletePerson.setBounds(60,180,100,30);
				deletePerson.setBackground(MAROON);
				deletePerson.setForeground(Color.WHITE);
				deletePerson.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						brain.deleteProduct(new product(Integer.parseInt(CNPBoxD.getText()),""));
						customerBuilder cb = new customerBuilder();
						DefaultTableModel model = cb.Model();
						populate(model,brain.viewCustomers());
						table.setModel(model);
						table.repaint();
					}
				});
				products.add(deletePerson);
				//------//
				
				products.setResizable(false);
				products.setVisible(true);
				people.dispose();
				order.dispose();
			}
		});
		controlPane.add(accountButton);
		
		JButton saveAndExitButton = new JButton("Quit");
		saveAndExitButton.setBounds(100,400,100,35);
		saveAndExitButton.setBackground(MAROON);
		saveAndExitButton.setForeground(Color.WHITE);
		saveAndExitButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				bank.dispose();
				people.dispose();
				products.dispose();
				order.dispose();
			}
		});
		controlPane.add(saveAndExitButton);
		
		mainPane.add(controlPane);
		//------//
		bank.add(mainPane);
		bank.setResizable(false);
		bank.setVisible(true);
	}
}

