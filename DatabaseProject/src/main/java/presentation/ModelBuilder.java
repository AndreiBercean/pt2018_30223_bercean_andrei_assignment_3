package presentation;

import java.lang.reflect.*;
import javax.swing.table.DefaultTableModel;

public class ModelBuilder <T> 
{
	private final Class<T> type;
	
	public ModelBuilder()
	{
		type = (Class<T>)((ParameterizedType)(Object) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public DefaultTableModel Model()
	{
		DefaultTableModel model = new DefaultTableModel();
		for (Field field : type.getDeclaredFields()) 
		{
			model.addColumn(field.getName());
		}
		return model;
	}
}
