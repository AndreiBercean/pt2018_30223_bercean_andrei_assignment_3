package businessLayer;

import java.util.ArrayList;
import java.util.List;

import dataAccessLayer.*;
import model.*;

public class Bridge 
{
	CustomerDAO cli = new CustomerDAO();
	ProductDAO prod = new ProductDAO();
	OrderDAO ord = new OrderDAO();
	StockDAO stk = new StockDAO();
	
	
//------------------------------------------------------------------//	
	//--==CLIENT==--//
	public void addCustomer(customer c)
	{
		cli.INSERT(c);
	}
	
	public void editCustomer(customer c)
	{
		cli.UPDATE(c, c.id);
	}
	
	public void deleteCustomer(customer c)
	{
		cli.DELETE(c.id);
	}
	
	//--==PRODUCT==--//
	public void addProduct(product p)
	{
		prod.INSERT(p);
	}
	
	public void editProduct(product p, stock s)
	{
		prod.UPDATE(p, p.id);
		if(stk.SELECT(p.id).isEmpty())stk.INSERT(new stock(p.id,0));
		stk.UPDATE(s, p.id);
	}
	
	public void deleteProduct(product p)
	{
		stk.DELETE(p.id);
		prod.DELETE(p.id);
	}
	
	//--==ORDER==--//
	public String order(product p, customer c, int q)
	{
		stock s = stk.SELECT(p.id).get(0);
		int ammount = s.ammount;
		if(ammount < q) return "Stoc insuficient";
		else
		{
			ammount -= q;
			ord.INSERT(new orders(0,c.id,p.id,q));
		}
		stk.UPDATE(new stock(0,ammount), p.id);
		return "Comanda inregistrata";
	}
	
	//--==VIEWS==--//
	public ArrayList<String[]> viewCustomers()
	{
		ArrayList<String[]> rez = new ArrayList<String[]>();
		List<customer> returnList = cli.SELECT_ALL();
		for(customer c : returnList)
		{
			rez.add(c.toStringVector());
		}
		return rez;
	}
	
	public ArrayList<String[]> viewProducts()
	{
		ArrayList<String[]> rez = new ArrayList<String[]>();
		List<product> returnList = prod.SELECT_ALL();
		for(product c : returnList)
		{
			rez.add(c.toStringVector());
		}
		return rez;
	}
	
	public ArrayList<String[]> viewOrders()
	{
		ArrayList<String[]> rez = new ArrayList<String[]>();
		List<orders> returnList = ord.SELECT_ALL();
		for(orders c : returnList)
		{
			rez.add(c.toStringVector());
		}
		return rez;
	}
	
	public ArrayList<String[]> viewStocks()
	{
		ArrayList<String[]> rez = new ArrayList<String[]>();
		List<stock> returnList = stk.SELECT_ALL();
		for(stock c : returnList)
		{
			rez.add(c.toStringVector());
		}
		return rez;
	}
}
