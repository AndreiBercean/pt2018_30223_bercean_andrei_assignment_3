package model;

public class product 
{
	public int id;
	public String name;
	
	public product(int id, String n)
	{
		this.id = id;
		name = n;
	}
	
	public product(){}
	
	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String[] toStringVector()
	{
		String[] aux = {"",""};
		aux[0] = id+"";
		aux[1] = name;
		return aux;
	}
}
