package model;

public class orders 
{
	public int id;
	public int id_customer;
	public int id_product;
	public int quantity;
	
	public orders(int id, int cus, int prod, int quan)
	{
		this.id = id;
		id_customer = cus;
		id_product = prod;
		quantity = quan;
	}
	
	public orders(){}
	
	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public int getId_customer() {
		return id_customer;
	}

	public void setId_customer(int id_customer) {
		this.id_customer = id_customer;
	}

	public int getId_product() {
		return id_product;
	}

	public void setId_product(int id_product) {
		this.id_product = id_product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String[] toStringVector()
	{
		String[] aux = {"","","",""};
		aux[0] = id+"";
		aux[1] = id_customer+"";
		aux[2] = id_product+"";
		aux[3] = quantity+"";
		return aux;
	}
}
