package model;

public class stock 
{
	public int id;
	public int ammount;
	
	public stock(int id, int am)
	{
		this.id = id;
		ammount = am;
	}
	
	public stock(){}
	
	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public int getAmmount() {
		return ammount;
	}

	public void setAmmount(int ammount) {
		this.ammount = ammount;
	}
	
	public String[] toStringVector()
	{
		String[] aux = {"",""};
		aux[0] = id+"";
		aux[1] = ammount+"";
		return aux;
	}
}
