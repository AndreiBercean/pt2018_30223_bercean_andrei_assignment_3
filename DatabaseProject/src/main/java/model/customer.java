package model;

public class customer 
{
	public int id;
	public String name;
	public String email;
	
	public customer(int id, String n, String e)
	{
		this.id = id;
		name = n;
		email = e;
	}
	
	public customer(){}
	
	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String[] toStringVector()
	{
		String[] aux = {"","",""};
		aux[0] = id+"";
		aux[1] = name;
		aux[2] = email;
		return aux;
	}
}
